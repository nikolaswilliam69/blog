@extends('layouts.app')
<style type="text/css">
    .avatar{
        border-radius: 100%;
        max-width: 100px;
    }
</style>
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
        @if(count($errors)>0)
            @foreach($errors->all() as $error)
                <div class = "alert alert-danger">{{$error}}</div>
            @endforeach
        @endif

        @if(session('response'))
            <div class = "alert alert-success">{{session('response')}}</div>
        @endif
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    <div class="col-md-4"> 
                    @if(!empty($profile))
                    <img src="{{ $profile->profile_image }}" class="avatar" alt="avatar">
                    @else
                    <img src="{{ url('images/avatar.png') }}" class="avatar" alt="avatar">
                    @endif
                    @if(!empty($profile))
                    <p class="lead">{{ $profile->name }}</p>
                    @else
                    <p></p>
                    @endif
                    @if(!empty($profile))
                    <p>{{ $profile->designation }}</p>
                    @else
                    <p></p>
                    @endif
                    </div>
                    <div class="col-md-8">
                    @if(count($posts)>0)
                        @foreach($posts->all() as $post)
                            <h4>{{$post->post_title}}</h4>
                            <img src="{{$post->post_image}}" alt="post image" width="50%" height="50%">
                            <p>{{$post->post_body}}</p>
                            <ul class="nav nav-pills">
                                <li role="presentation">
                                <a href='{{url("/view/{$post->id}")}}'><span>View</span></a>
                                </li>
                                <li role="presentation">
                                <a href='{{url("/edit/{$post->id}")}}'><span>&emsp;Edit</span></a>
                                </li>
                                <li role="presentation">
                                <a href='{{url("/delete/{$post->id}")}}'><span>&emsp;Delete</span></a>
                                </li>
                            </ul>
                            <cite style="float:left">Posted On: {{date('M j, Y H:i', strtotime($post->updated_at))}}</cite>
                            <hr/>
                        @endforeach
                    @else
                        <p>No Posts Available to read right now</p>
                    @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
