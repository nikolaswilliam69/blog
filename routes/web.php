<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/addpost', 'PostController@post');

Route::get('/profile', 'ProfileController@profile');

Route::get('/category', 'CategoryController@category');

Route::post('/addCategory','CategoryController@addCategory');

Route::post('/addProfile', 'ProfileController@addProfile');

Route::post('/addpost', 'PostController@addpost');

Route::get('/view/{id}', 'PostController@view');

Route::get('/edit/{id}', 'PostController@edit');

Route::get('/delete/{id}', 'PostController@delete');